<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="I_RESET" />
        <signal name="XLXN_2" />
        <signal name="I_SW(3:0)" />
        <signal name="I_BUTTON(3:0)" />
        <signal name="O_HSYNC" />
        <signal name="O_VSYNC" />
        <signal name="O_AUDIO_L" />
        <signal name="O_AUDIO_R" />
        <signal name="O_VIDEO_R(3:0)" />
        <signal name="O_VIDEO_G(3:0)" />
        <signal name="O_VIDEO_B(3:0)" />
        <signal name="O_LED(3:0)" />
        <signal name="clk" />
        <port polarity="Input" name="I_RESET" />
        <port polarity="Input" name="I_SW(3:0)" />
        <port polarity="Input" name="I_BUTTON(3:0)" />
        <port polarity="Output" name="O_HSYNC" />
        <port polarity="Output" name="O_VSYNC" />
        <port polarity="Output" name="O_AUDIO_L" />
        <port polarity="Output" name="O_AUDIO_R" />
        <port polarity="Output" name="O_VIDEO_R(3:0)" />
        <port polarity="Output" name="O_VIDEO_G(3:0)" />
        <port polarity="Output" name="O_VIDEO_B(3:0)" />
        <port polarity="Output" name="O_LED(3:0)" />
        <port polarity="Input" name="clk" />
        <blockdef name="PACMAN">
            <timestamp>2011-4-4T10:39:35</timestamp>
            <rect width="496" x="64" y="-1280" height="1280" />
            <line x2="0" y1="-1248" y2="-1248" x1="64" />
            <line x2="0" y1="-848" y2="-848" x1="64" />
            <rect width="64" x="0" y="-460" height="24" />
            <line x2="0" y1="-448" y2="-448" x1="64" />
            <rect width="64" x="0" y="-60" height="24" />
            <line x2="0" y1="-48" y2="-48" x1="64" />
            <line x2="624" y1="-608" y2="-608" x1="560" />
            <line x2="624" y1="-544" y2="-544" x1="560" />
            <line x2="624" y1="-480" y2="-480" x1="560" />
            <line x2="624" y1="-416" y2="-416" x1="560" />
            <rect width="64" x="560" y="-300" height="24" />
            <line x2="624" y1="-288" y2="-288" x1="560" />
            <rect width="64" x="560" y="-236" height="24" />
            <line x2="624" y1="-224" y2="-224" x1="560" />
            <rect width="64" x="560" y="-172" height="24" />
            <line x2="624" y1="-160" y2="-160" x1="560" />
            <rect width="64" x="560" y="-108" height="24" />
            <line x2="624" y1="-96" y2="-96" x1="560" />
        </blockdef>
        <blockdef name="Clock">
            <timestamp>2015-1-20T5:11:46</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
        </blockdef>
        <block symbolname="PACMAN" name="XLXI_1">
            <blockpin signalname="I_RESET" name="I_RESET" />
            <blockpin signalname="XLXN_2" name="I_CLK_REF" />
            <blockpin signalname="I_SW(3:0)" name="I_SW(3:0)" />
            <blockpin signalname="I_BUTTON(3:0)" name="I_BUTTON(3:0)" />
            <blockpin signalname="O_HSYNC" name="O_HSYNC" />
            <blockpin signalname="O_VSYNC" name="O_VSYNC" />
            <blockpin signalname="O_AUDIO_L" name="O_AUDIO_L" />
            <blockpin signalname="O_AUDIO_R" name="O_AUDIO_R" />
            <blockpin signalname="O_VIDEO_R(3:0)" name="O_VIDEO_R(3:0)" />
            <blockpin signalname="O_VIDEO_G(3:0)" name="O_VIDEO_G(3:0)" />
            <blockpin signalname="O_VIDEO_B(3:0)" name="O_VIDEO_B(3:0)" />
            <blockpin signalname="O_LED(3:0)" name="O_LED(3:0)" />
        </block>
        <block symbolname="Clock" name="XLXI_4">
            <blockpin signalname="clk" name="CLKIN_IN" />
            <blockpin name="LOCKED_OUT" />
            <blockpin signalname="XLXN_2" name="CLKFX_OUT" />
            <blockpin name="CLKIN_IBUFG_OUT" />
            <blockpin name="CLK0_OUT" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="5440" height="7040">
        <instance x="3168" y="3760" name="XLXI_1" orien="R0">
        </instance>
        <branch name="I_RESET">
            <wire x2="3168" y1="2512" y2="2512" x1="3136" />
        </branch>
        <branch name="XLXN_2">
            <wire x2="3136" y1="2848" y2="2848" x1="2560" />
            <wire x2="3136" y1="2848" y2="2912" x1="3136" />
            <wire x2="3168" y1="2912" y2="2912" x1="3136" />
        </branch>
        <branch name="I_SW(3:0)">
            <wire x2="3168" y1="3312" y2="3312" x1="3136" />
        </branch>
        <branch name="I_BUTTON(3:0)">
            <wire x2="3168" y1="3712" y2="3712" x1="3136" />
        </branch>
        <branch name="O_HSYNC">
            <wire x2="3824" y1="3152" y2="3152" x1="3792" />
        </branch>
        <branch name="O_VSYNC">
            <wire x2="3824" y1="3216" y2="3216" x1="3792" />
        </branch>
        <branch name="O_AUDIO_L">
            <wire x2="3824" y1="3280" y2="3280" x1="3792" />
        </branch>
        <branch name="O_AUDIO_R">
            <wire x2="3824" y1="3344" y2="3344" x1="3792" />
        </branch>
        <branch name="O_VIDEO_R(3:0)">
            <wire x2="3824" y1="3472" y2="3472" x1="3792" />
        </branch>
        <branch name="O_VIDEO_G(3:0)">
            <wire x2="3824" y1="3536" y2="3536" x1="3792" />
        </branch>
        <branch name="O_VIDEO_B(3:0)">
            <wire x2="3824" y1="3600" y2="3600" x1="3792" />
        </branch>
        <branch name="O_LED(3:0)">
            <wire x2="3824" y1="3664" y2="3664" x1="3792" />
        </branch>
        <iomarker fontsize="28" x="3136" y="2512" name="I_RESET" orien="R180" />
        <iomarker fontsize="28" x="3136" y="3312" name="I_SW(3:0)" orien="R180" />
        <iomarker fontsize="28" x="3136" y="3712" name="I_BUTTON(3:0)" orien="R180" />
        <iomarker fontsize="28" x="3824" y="3152" name="O_HSYNC" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3216" name="O_VSYNC" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3280" name="O_AUDIO_L" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3344" name="O_AUDIO_R" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3472" name="O_VIDEO_R(3:0)" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3536" name="O_VIDEO_G(3:0)" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3600" name="O_VIDEO_B(3:0)" orien="R0" />
        <iomarker fontsize="28" x="3824" y="3664" name="O_LED(3:0)" orien="R0" />
        <branch name="clk">
            <wire x2="2096" y1="2784" y2="2784" x1="2064" />
        </branch>
        <iomarker fontsize="28" x="2064" y="2784" name="clk" orien="R180" />
        <instance x="2096" y="3008" name="XLXI_4" orien="R0">
        </instance>
    </sheet>
</drawing>